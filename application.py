from flask import Flask, render_template, redirect, request, url_for, session
from werkzeug.security import generate_password_hash, check_password_hash
from flaskext.mysql import MySQL
from flask_mail import Mail, Message
from bs4 import BeautifulSoup

import boto3
from botocore.exceptions import ClientError

from operator import itemgetter
import subprocess
import datetime
import top_songs
import spotipy.util as util
import spotipy
import urllib
import requests
import uuid
import json
import time
import re
import os

path = os.getcwd()

mysql = MySQL()
mail = Mail()

application = Flask(__name__)
application.secret_key = 'gqaD4Ak941q/z5yEDSxHmTizw54LQ0ey+zBFdJoK6vc='
application.config['AWS_ACCESS_KEY_ID'] = 'AKIAIT4HCWLHOKEMDVQA'
application.config['AWS_SECRET_ACCESS_KEY'] = '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'

application.config['MYSQL_DATABASE_USER'] = 'rhmd'
application.config['MYSQL_DATABASE_PASSWORD'] = '83hXc81*'
application.config['MYSQL_DATABASE_DB'] = 'playlist'
application.config['MYSQL_DATABASE_HOST'] = 'rh-playlist.ch6gqh9voru6.us-west-2.rds.amazonaws.com'
mysql.init_app(application)

application.config['MAIL_SERVER'] = 'smtp.gmail.com'
application.config['MAIL_PORT'] = 465
application.config['MAIL_USE_TLS'] = False
application.config['MAIL_USE_SSL'] = True
application.config['MAIL_USERNAME'] = "rhplaylist0@gmail.com"
application.config['MAIL_PASSWORD'] = "83hXc81*"
mail.init_app(application)

SENDER = "rhplaylist0@gmail.com"
AWS_REGION = "us-west-2"
CHARSET = "UTF-8"

client = boto3.client('ses', region_name=AWS_REGION,
    aws_access_key_id=application.config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=application.config['AWS_SECRET_ACCESS_KEY']
)

conn = mysql.connect()
cursor = conn.cursor()

clientId = '644905e26d794a65b5ad34118486bf2b'
clientSecret = 'a1f1f4a5de964fb6b509d79c6da71a1f'
clientRedirect = 'http://localhost/callback'

username = 'jsteeves005'
scope = 'user-library-read'

os.environ["SPOTIPY_CLIENT_ID"] = clientId
os.environ["SPOTIPY_CLIENT_SECRET"] = clientSecret
os.environ["SPOTIPY_REDIRECT_URI"] = clientRedirect

def update_conn(timeout=0):
    global conn, cursor
    now = datetime.datetime.now().strftime("%m/%d/%Y %I:%M")
    with open(path + "/conn_test.log", "w") as f:
        f.write(now + "\nUpdating db connection.\n\n")
    f.closed
    try:
        cursor.execute("SELECT local FROM playlist_items WHERE id=2")
    except:
        conn.close()
        conn = mysql.connect()
        cursor = conn.cursor()

def safe_send_email(recipients, sender, subject, text, html, retry=1):
    global client
    try:
        response = client.send_email(
            Destination={ 'ToAddresses': recipients },
            Message={
                'Body': {
                    'Text': {
                        'Charset': CHARSET,
                        'Data': text,
                    },
                    'Html': {'Data': html}
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=sender,
        )
    except ClientError as e:
        if retry:
            client = boto3.client('ses', region_name=AWS_REGION,
                aws_access_key_id=application.config['AWS_ACCESS_KEY_ID'],
                aws_secret_access_key=application.config['AWS_SECRET_ACCESS_KEY']
            )
            return safe_send_email(recipients, sender, subject,
                                        text, html, retry=0)
        else:
            return e.response['Error']['Message']
    else:
        return ("Email sent! Message ID:\n" + 
            response['ResponseMetadata']['RequestId'])

@application.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template("login.html", register=0, error="")
    elif request.method == 'POST':
        symbol = request.json['symbol'].lower()
        password = request.json['password']

        cmd = "SELECT password, user_type FROM users WHERE username=%s"
        try:
            cursor.execute(cmd, (symbol,))
        except:
            update_conn()
            cursor.execute(cmd, (symbol,))

        cmd_data = cursor.fetchone()
        if cmd_data:
            pass_hash = cmd_data[0]
            perm = cmd_data[1]
        else:
            return "user not found"

        if check_password_hash(pass_hash, password):
            session['user'] = symbol
            session['perm'] = perm
            return "done"
        else:
            return "incorrect password" 

@application.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'GET':
        return render_template("login.html", register=1, error="")
    elif request.method == 'POST':
        symbol = request.json["symbol"].lower()
        email = request.json["email"]
        password = request.json["password"]
        pass_hash = generate_password_hash(password)

        symbol_cmd = "SELECT * FROM users WHERE username=%s"
        try:
            cursor.execute(symbol_cmd, (symbol,))
        except:
            update_conn()
            cursor.execute(symbol_cmd, (symbol,))

        symbol_check = cursor.fetchall()
        if symbol_check:
            return "symbol"
        
        email_cmd = "SELECT * FROM users WHERE email=%s"
        try:
            cursor.execute(email_cmd, (email,))
        except:
            update_conn()
            cursor.execute(email_cmd, (email,))
        email_check = cursor.fetchall()
        if email_check:
            return "email"
    
        cmd = "INSERT INTO users VALUES(0, %s, %s, %s, NULL, NULL, NULL)" 
        try:
            cursor.execute(cmd, (symbol, email, pass_hash))
        except:
            update_conn()
            cursor.execute(cmd, (symbol, email, pass_hash))
        conn.commit()

        with open('verification.json', 'r') as f:
            codes = json.load(f)
        f.closed

        verify = uuid.uuid4()
        unique = False
        while not unique:
            try:
                tmp = codes[str(verify)]
            except:
                unique = True
                codes[str(verify)] = symbol
            else:
                verify = uuid.uuid4()

        subject = "RH Playlist Verification"
        text = "Click this link or visit the page in your browser to verify: "
        html = text + "<a href='https://rhplaylist.com/verify/{0}'>https://rhplaylist.com/verify/{0}</a>".format(verify)
        text += "https://rhplaylist.com/verify/{0}"
        output = safe_send_email([email], SENDER, subject, text, html)

        if "Email sent!" not in output:
            remove = "DELETE FROM users WHERE username=%s"
            try:
                cursor.execute(remove, (symbol,))
            except:
                update_conn()
                cursor.execute(remove, (symbol,))
            conn.commit()
            return "bad email"
        else:
            with open('verification.json', 'w') as f:
                json.dump(codes, f)
            f.closed

        session['user'] = symbol
        return "registered"

@application.route('/complete')
def completed():
    msg = "Registration complete!"
    return render_template("msg_form.html", title="Registration Complete", message=msg)

@application.route('/logout')
def logout():
    session['user'] = None 
    session['perm'] = None
    return redirect(request.referrer) or redirect(url_for('playlist'))
    
@application.route("/verify/<code>/")
def verify(code):
    with open("verification.json", "r") as f:
        codes = json.load(f)
    f.closed

    try:
        symbol = codes[code] 
    except:
        title = "Invalid Code"
        msg = "Invalid verification code."
        return render_template('msg_form.html', title=title, message=msg)

    if session['user'] == symbol:
        del codes[code]
        with open("verification.json", "w") as f:
            json.dump(codes, f)
        f.closed

        cmd = "UPDATE users SET verified=1 WHERE username=%s"
        try:
            cursor.execute(cmd, (symbol,))
        except:
            conn_update()
            cursor.execute(cmd, (symbol,))
        conn.commit()

        title = "Verified"
        msg = "{} has been successfully verified.".format(symbol)
        return render_template('msg_form.html', title=title, message=msg)
    else:
        title = "Must Log In"
        msg = "You must be logged in to verify your account."
        return render_template('msg_form.html', title=title, message=msg)

@application.route('/getTracks/<item_id>', methods=['POST'])
def getTracks(item_id):
    try:
        item_id = int(item_id)
    except:
        return "invalid"
    cmd = "SELECT * FROM `" + str(item_id) + "`"
    try:
        cursor.execute(cmd)
    except:
        update_conn()
        cursor.execute(cmd)
    tracks = list(cursor.fetchall())
    tracks = sorted(tracks, key=itemgetter(0))

    plays = 0
    track_plays = []
    for track in tracks:
        track_plays.append(track[2]) 
        plays += track[2]

    track_data = {"plays": plays, "tracks": tracks, "track_plays": track_plays}

    return json.dumps(track_data)

sort_list = ['id', 'album','artist']
@application.route('/playlist/')
@application.route('/playlist/<archive>')
def playlist(archive=None):
    cmd = "SELECT * FROM playlist_items"
    if not archive:
        cmd += " WHERE current=1"
    else:
        cmd += " WHERE current=0"
    try:
        cursor.execute(cmd)
    except:
        update_conn()
        cursor.execute(cmd)
    data = list(cursor.fetchall())

    sort_key = request.args.get('sort')
    sort_index = 1
    if sort_key:
        if sort_key in ["newest", "oldest"]:
            sort_index = 8
        else:
            try:
                sort_index = sort_list.index(sort_key)
            except:
                sort_index = 1
                sort_key = "album"
    else:
        sort_key = "album"
    
    if sort_index == 8:
        reverse = True if sort_key == "newest" else False
        data.sort(key=lambda x: datetime.datetime.strptime(x[8], "%m-%d-%Y"), reverse=reverse)
    else:
        data.sort(key=lambda x: x[sort_index].lower())

    return render_template("playlist.html", playlist=data, admin=0, archive=archive, sort=sort_key)

@application.route('/admin/playlist/<archive>')
@application.route('/admin/playlist')
def admin_playlist(archive=None):
    if session['perm'] != "MD":
        return redirect(url_for('playlist'))
    
    cmd = "SELECT * FROM playlist_items WHERE current=%s"
    current = "0" if archive else "1"

    try:
        cursor.execute(cmd, (current,))
    except:
        update_conn()
        cursor.execute(cmd, (current,))
    data = list(cursor.fetchall())

    sort_key = request.args.get('sort')
    sort_index = 1
    if sort_key:
        if sort_key in ["newest", "oldest"]:
            sort_index = 8
        else:
            try:
                sort_index = sort_list.index(sort_key)
            except:
                sort_index = 1
                sort_key = "album"
    else:
        sort_key = "album"
    
    if sort_index == 8:
        reverse = True if sort_key == "newest" else False
        data.sort(key=lambda x: datetime.datetime.strptime(x[8], "%m-%d-%Y"), reverse=reverse)
    else:
        data.sort(key=lambda x: x[sort_index].lower())

    return render_template("playlist.html", playlist=data, admin=1, archive=archive, sort=sort_key)

@application.route('/accounts')
def accounts():
    if session['perm'] != "MD":
        return redirect(url_for('playlist'))

    cmd = "SELECT * FROM users"
    try:
        cursor.execute(cmd)
    except:
        update_conn()
        cursor.execute(cmd)
    accounts = cursor.fetchall()
    return render_template("accounts.html", accounts=accounts)

def date_sort(x):
    return datetime.datetime.strptime(x, '%m_%d_%Y.json')

@application.route('/archives')
def archives():
    archives = os.listdir(path + url_for("static", filename="archives"))
    archives = sorted(archives, key=date_sort, reverse=True)
    return render_template('archive_select.html', archives=archives)

@application.route('/')
@application.route('/archive/<file_date>')
def top20(file_date=None):
    if file_date:
        with open(path + url_for("static", filename="archives/" + file_date), "r") as f:
            top20 = json.load(f)
        f.closed
        try:
            with open(path + url_for("static", filename="archives/" + file_date), "r") as f:
                top20 = json.load(f)
            f.closed
        except:
            return render_template('msg_form.html', title="Error 404", message="Sorry, this page doesn't exist!")
        else:
            start_string = file_date[0:-5].replace("_","/")
            start = datetime.datetime.strptime(start_string, "%m/%d/%Y")
            end = datetime.datetime.strptime(top20['end'], "%m/%d/%Y")
            end_date = top20['end'] if (end - start).days > 7 else None
            return render_template("top.html", playlist=top20['top-songs'], file_date=start_string, end_date=end_date)

    reload(top_songs)
    start = datetime.datetime.strptime(top_songs.start, "%m/%d/%Y")
    end = datetime.datetime.strptime(top_songs.end, "%m/%d/%Y")
    end_date = top_songs.end if (end - start).days > 7 else None
    return render_template("top.html", playlist=top_songs.top20, file_date=top_songs.start, end_date=end_date)

@application.route('/viewlog')
def viewlog():
    if session['perm'] != "MD":
        return redirect(url_for('playlist'))
    
    try:
        with open('playlist.log', 'r') as f:
            log = f.readlines()
        f.closed
    except:
        log = ""
        chart = ""
    else:
        albums = {}
        artists = {}
        date_regex = re.compile(r" [(].*[)](\n)?", re.IGNORECASE)
        name_regex = re.compile(r".*: ", re.IGNORECASE)
        for line in log:
            line = date_regex.sub("", line)
            line = name_regex.sub("", line)
            if line and line != "\n":
                fields = line.split(' - ')
                album = fields[0]
                artist = fields[1]
                try:
                    plays = albums[album]
                except:
                    albums[album] = 1
                    artists[album] = artist
                else:
                    albums[album] = plays + 1
            
        raw_chart = sorted(albums.iteritems(), key=itemgetter(1), reverse=True)
        chart = []
        for entry in raw_chart:
            artist = artists[entry[0]]
            chart.append((entry[0], artist, entry[1], ""))

    start_parts = top_songs.end.split("/")
    start = start_parts[2] + "-" + start_parts[0] + "-" + start_parts[1]

    end = datetime.datetime.now().strftime("%Y-%m-%d")

    return render_template("msg_form.html", title="Top 20 Log", message="\n".join(log), log=1, chart=chart, start=start, end=end)


@application.route('/edit20')
def edit20():
    if session['perm'] != "MD":
        return redirect(url_for('playlist'))

    reload(top_songs)
    top20 = top_songs.top20
    
    featured_songs = {}
    for entry in top20:
        featured_songs[str(entry[0])] = entry[9]

    return render_template("edit_top.html", top_twenty=top20, featured_songs=featured_songs)

@application.route('/updateSong', methods=['POST'])
def updateSong():
    if session['perm'] != "MD":
        return "permission" 

    entry_id = request.json["id"]
    new_song = request.json["song"]
    top20 = top_songs.top20
    for i in range(len(top20)):
        if str(top20[i][0]) == entry_id:
            top20[i][9] = new_song
    
    with open("top_songs.py", "w") as f:
        f.write("end = \"" + top_songs.end + "\"\nstart = \"" + top_songs.start + "\"\ntop20 = " + str(top20)) 
    f.closed
    reload(top_songs)

    return "done"

@application.route('/make20', methods=['POST'])
def make20():
    if session['perm'] != "MD":
        return "permission" 

    try:
        reload(top_songs)
        start_date = top_songs.start
        end_date = top_songs.end
        file_date = start_date.replace("/","_")
        archive = {'top-songs': top_songs.top20, 'end': end_date}
        with open(path + url_for('static', filename='archives/' + file_date + '.json'), 'w') as f:
            json.dump(archive, f)
        f.closed
    except:
        pass

    try:
        cursor.execute("SELECT * FROM playlist_items WHERE current=1")
    except:
        update_conn()
        cursor.execute("SELECT * FROM playlist_items WHERE current=1")

    data = cursor.fetchall()
    top20 = []
    #min_plays = None
    #for entry in data:
    #    plays = entry[3]
    #    if min_plays == None:
    #        min_plays = plays
    #    if len(top20) < 20:
    #        if min_plays > plays:
    #            min_plays = plays
    #        top20.append(list(entry))
    #    elif plays > min_plays:
    #        top20.sort(key=itemgetter(3), reverse=True)
    #        top20[19] = list(entry)
    #top20.sort(key=itemgetter(3), reverse=True)
    chart = request.json["chart"]
    for chart_item in chart:
        item = chart_item.split("@^$*@")
        length = len(top20)
        for entry in data:
            if entry[1] == item[0] and entry[2] == item[1]:
                top20.append(list(entry))
        if len(top20) == length:
            for entry in data:
                if item[0] in entry[1] and item[1] in entry[2]:
                    top20.append(list(entry))

    for song in top20:
        album_table = song[0]
        
        cmd = "SELECT * FROM `%s`"
        try:
            cursor.execute(cmd, (album_table))
        except:
            update_conn()
            cursor.execute(cmd, (album_table))
        tracks = cursor.fetchall()
        
        pop_song = None
        for track in tracks:
            if not pop_song:
                pop_song = (track[1], track[2])
            elif track[2] > pop_song[1]:
                pop_song = (track[1], track[2])
        
        song.append(pop_song[0])
        
    reload(top_songs)

    end = request.json["end"]
    start = request.json["start"]
    
    with open("top_songs.py", "w") as f:
        f.write("end = \"" + end + "\"\nstart = \"" + start + "\"\ntop20 = " + str(top20)) 
    f.closed

    for entry in data:
        clear_cmd = "UPDATE `{}` SET plays=0 WHERE track>0".format(entry[0])
        try:
            cursor.execute(clear_cmd)
            conn.commit()
        except:
            update_conn()
            try:
                cursor.execute(clear_cmd)
                conn.commit()
            except:
                pass
    genclear = "UPDATE playlist_items SET plays=0 WHERE id>0"
    try:
        cursor.execute(genclear)
    except:
        update_conn()
        cursor.execute(genclear)
    conn.commit()

    log_file = path + "/static/logs/" + top_songs.start.replace("/","_") + ".log"
    p = subprocess.Popen(["mv", path + "/playlist.log", log_file])

    return "done" 
            
def lookup(artist, album):
    token = util.prompt_for_user_token(username, scope)
    sp = spotipy.Spotify(auth=token)
    results = sp.search(q='artist:' + artist, type='artist')
    items = results['artists']['items']
    if len(items) > 0:
        artist_id = items[0]['id']
        album_results = sp.artist_albums(artist_id, album_type='album')['items']
        albums = []
        for album_result in album_results:
            image = album_result['images'][0]['url']
             
            if not albums:
                albums.append((album_result['name'], album_result['id'], image)) 

            add = True
            for item in albums:
                if album_result['name'] == item[0]: 
                    add = False
                    break

            if add:
                albums.append((album_result['name'], album_result['id'], image)) 

        for index, item in enumerate(albums):
            try:
                album_name = item[0].encode("ascii")
                if album in album_name:
                    albums.insert(0, albums.pop(index))
                    break
            except:
                pass

        return albums
    else:
        return None

@application.route('/albumselect')
def albumselect():
    artist = session[session['user'] + "-artist"]
    album = session[session['user'] + "-album"]
    albums = session[session['user'] + "-albums"]
    return render_template('album_select.html', artist=artist, album=album, albums=albums)
    

@application.route('/add', methods=['GET', 'POST'])
def add():
    if not request.args.get('msg'):
        message = ""
    else:
        message = request.args.get('msg')

    if not request.args.get('album'):
        album = ""
    else:
        album = request.args.get('album')

    if request.method == 'POST':
        if session['perm'] != "MD":
            return redirect(url_for('playlist'))

        artist = request.json["artist"]
        album = request.json["album"]
        tracks = None
        release_date = request.json["date"]

        rel_dates = release_date.split('-')
        release_date = "{}-{}-{}".format(rel_dates[1], rel_dates[2], rel_dates[0])

        try:
            label = request.json["label"]
        except:
            label = "NULL"

        try:
            location = request.json["location"]
        except:
            location = "NULL"

        local = request.json["local"]
        if local:
            session[session['user'] + '-local'] = "1"
        else:
            session[session['user'] + '-local'] = "0"

        session[session['user'] + '-label'] = label
        session[session['user'] + '-location'] = location
        session[session['user'] + '-date'] = release_date

        albums = lookup(artist, album)
        if not albums:
            albums = []

        session[session['user'] + '-artist'] = artist
        session[session['user'] + '-album'] = album
        session[session['user'] + '-albums'] = albums

        return "done"
    
    return render_template("add_form.html", message=message, album=album) 

@application.route('/insert/<method>', methods=['POST'])
def insert(method):
    if session['perm'] != "MD":
        return "failed permission"

    album = request.json['album']
    artist = request.json['artist']
    tracks = []
    cover_url = None

    if method == "spotify":
        auth = {"Authorization": "Bearer {}".format(clientSecret),
                "Content-Type": 'application/x-www-form-urlencoded'}
        album_id = request.json['album_id']
        cover_url = request.json['image']
        tracks_req = requests.get("https://api.spotify.com/v1/albums/{}/tracks"
                                        .format(album_id), headers=auth)
        track_json = tracks_req.json()['items']
        tracks = []
        for track in track_json:
            tracks.append(track['name'])

    if method == "bandcamp":
        url = request.json['url']
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        r = requests.get(url, headers=headers)
        soup = BeautifulSoup(r.text, "html.parser")
        info = soup.find(id="name-section")
        cover_url = soup.find(id="tralbumArt").find("a", href=True)['href']
        span = info.find("span")
        bc_album = info.find(class_="trackTitle").contents[0].strip()
        bc_artist = span.find("a").contents[0].strip()

        if bc_album:
            album = bc_album
        if bc_artist:
            artist = bc_artist

        track_table = soup.find(id="track_table")
        track_elements = track_table.find_all("tr")
        for track_hold in track_elements:
            try:
                title_hold = track_hold.find(class_="title-col").find(class_="title").find("a").find("span")
                title = title_hold.contents[0].strip()
                tracks.append(title)
            except:
                pass

    cmd = "SELECT * FROM playlist_items WHERE artist=%s AND album=%s"
    try:
        cursor.execute(cmd, (artist, album))
    except:
        update_conn()
        cursor.execute(cmd, (artist, album))
    data = cursor.fetchone()
    
    if not data:
        label_storage = session['user'] + '-label'
        location_storage = session['user'] + '-location'
        local_storage = session['user'] + '-local'
        date_storage = session['user'] + '-date'
        cmd = "INSERT INTO playlist_items VALUES(0, %s, %s, 0, %s, %s, %s, 1, %s, NOW())"
        try:
            cursor.execute(cmd, (album, artist, session[label_storage], 
                            session[location_storage], 
                            session[local_storage],
                            session[date_storage]))
        except:
            update_conn()
            cursor.execute(cmd, (album, artist, session[label_storage], 
                            session[location_storage], 
                            session[local_storage],
                            session[date_storage]))
        conn.commit()
        session[label_storage] = "NULL"
        session[location_storage] = "NULL"
        session[local_storage] = "0"
        session[date_storage] = "NULL"
        
        song_table = str(cursor.lastrowid)
        cmd = "CREATE TABLE `{}` ( `track` INT NULL, `song` VARCHAR(75) NULL, `plays` INT NULL, PRIMARY KEY(track))".format(song_table)
        try:
            cursor.execute(cmd)
        except:
            update_conn()
            cursor.execute(cmd)

        conn.commit()

        if tracks:
            for number, track in enumerate(tracks):
                cmd = "INSERT INTO `{}` VALUES(%s, %s, 0)".format(song_table)
                try:
                    cursor.execute(cmd, (str(number + 1), track))
                except:
                    update_conn()
                    cursor.execute(cmd, (str(number + 1), track))
                conn.commit()

        if cover_url:
            r = requests.get(cover_url)
            with open(path + url_for("static", filename="img/" + song_table + ".jpg"), "w") as f:
                f.write(r.content)
            f.closed
    
        return song_table 
    else:
        return "duplicate"

@application.route('/drop/<album>', methods=['POST'])
def drop(album):
    if session['perm'] != "MD":
        return "failed permission"

    try:
        item_cmd = "DELETE FROM playlist_items WHERE id=%s"
        drop_cmd = "DROP TABLE `{}`".format(album)
        try:
            cursor.execute(item_cmd, (album,))
        except:
            update_conn()
            cursor.execute(item_cmd, (album,))
        try:
            cursor.execute(drop_cmd)
        except:
            update_conn()
            cursor.execute(drop_cmd)
        conn.commit()
    except:
        return "failed"
    else:
        return "done"


@application.route('/remove/<album>', methods=['POST'])
def remove(album):
    if session['perm'] != "MD":
        return "failed permission"

    try:
        cmd = "UPDATE playlist_items SET current=0 WHERE id=%s"
        try:
            cursor.execute(cmd, (album,)) 
        except:
            update_conn()
            cursor.execute(cmd, (album,)) 
        conn.commit() 
    except:
        return "failed"
    else:
        return "done"

weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
                'Sunday']
@application.route('/log', methods=['POST'])
def log():
    if not session['perm'] or not session['user']:
        return "failed permissions"

    album_id = request.json["album"]
    track = request.json["track"]

    try:
        cmd = "UPDATE `{}` SET plays = plays + 1 WHERE track=%s".format(album_id)
        cmd2 = "UPDATE playlist_items SET plays = plays + 1 WHERE id=%s"

        try:
            cursor.execute(cmd, (track,))
        except:
            update_conn()
            cursor.execute(cmd, (track,))
        try:
            cursor.execute(cmd2, (album_id,))
        except:
            update_conn()
            cursor.execute(cmd2, (album_id,))
        conn.commit()
        
        fetch = "SELECT album, artist FROM playlist_items WHERE id=%s"
        try:
            cursor.execute(fetch, (album_id,))
        except:
            update_conn()
            cursor.execute(fetch, (album_id,))
        item_data = cursor.fetchone()

        now = datetime.datetime.now()

        song_cmd = "SELECT song FROM `{}` WHERE track=%s".format(album_id)
        try:
            cursor.execute(song_cmd, (track,))
        except:
            update_conn()
            cursor.execute(song_cmd, (track,))
        song = cursor.fetchone()[0]

        with open("playlist.log", "a+") as log:
            log.write("{}: {} - {} - {} ({} - {}:{}, {}/{}/{})\n"
                .format(session['user'], item_data[0], item_data[1], song,
                        weekdays[now.weekday()], now.hour, now.minute, 
                        now.month, now.day, now.year))
        log.closed

    except:
        return "failed"
    else:
        return "done"

@application.route('/insertTracks', methods=['POST'])
def insert_tracks():
    if session['perm'] != "MD":
        return "failed permissions"

    new_tracks = request.json['tracks']
    album = request.json['album']

    for track in new_tracks:
        cmd = "INSERT INTO `{}` VALUES(%s, %s, 0)".format(album)
        try:
            try:
                cursor.execute(cmd, (track['number'], track['name']))
            except:
                update_conn()
                cursor.execute(cmd, (track['number'], track['name']))
            conn.commit()
        except:
            pass

    return "done"

@application.route('/info/<album>')
def album_info(album):
    cmd = "SELECT * FROM `{}`".format(album)
    try:
        cursor.execute(cmd)
        tracks = list(cursor.fetchall())
        tracks = sorted(tracks, key=itemgetter(0))

        cmd = "SELECT * FROM `playlist_items` WHERE id=%s"
        try:
            cursor.execute(cmd, (album))
        except:
            update_conn()
            cursor.execute(cmd, (album))

        album_info = cursor.fetchone()
    except:
        return render_template('msg_form.html', title="Error 404", message="Sorry, this page doesn't exist!")
    else:
        return render_template('album_info.html', album=album_info, tracks=tracks)

@application.route('/updateImage', methods=['POST'])
def updateImage():
    if session['perm'] != "MD":
        return "failed permissions"

    cover_url = request.json['url']
    album = request.json['album']

    try:
        r = requests.get(cover_url)
        with open(path + url_for("static", filename="img/" + album + ".jpg"), "w") as f:
            f.write(r.content)
        f.closed
        return "done"
    except:
        return "failed"
    else:
        return "done"

album_table = ['id', 'album', 'artist', 'plays', 'label', 'location', 'local', 'current', 'date']
user_table = ['id', 'username', 'email', 'password', 'user_type', 'verified', 'request']
@application.route('/update', methods=['POST'])
def update():
    if session['perm'] != "MD":
        return "failed permissions."

    index = request.json['index']
    id_val = request.json['id_val']
    new_val = request.json['new_val']
    table = request.json['table']
    if table == 'albums':
        index_table = album_table
        table_name = 'playlist_items'
    elif table == 'users':
        req_cmd = "UPDATE users SET request=0 WHERE id=%s"
        try:
            cursor.execute(req_cmd, (id_val,))
        except:
            update_conn()
            cursor.execute(req_cmd, (id_val,))
        conn.commit()
        index_table = user_table
        table_name = 'users'
        
    column = index_table[int(index)]
    if column == "verified":
        with open("verification.json", "r") as f:
            codes = json.load(f)
        f.closed
        symbol = request.json['symbol']
        for code in codes:
            if codes[code] == symbol:
                del codes[code]
                break
        with open('verification.json', 'w') as f:
            json.dump(codes, f)
        f.closed

    try:
        cmd = "UPDATE {} SET {}=%s WHERE id=%s".format(table_name, column)
        try:
            cursor.execute(cmd, (new_val, id_val))
        except:
            update_conn()
            cursor.execute(cmd, (new_val, id_val))
        conn.commit()
    except:
        return "failed"
    else:
        return "done"

@application.route('/delete', methods=['POST'])
def delete_track():
    if session['perm'] != "MD":
        return "failed permissions"
    
    album = request.json['album']
    track = request.json['track']

    try:
        cmd = "DELETE FROM `{}` WHERE track=%s".format(album)
        try:
            cursor.execute(cmd, (track,))
        except:
            update_conn()
            cursor.execute(cmd, (track,))
        conn.commit()
    except:
        return "failed"
    else:
        return "done"

@application.route('/admin')
def admin_page():
    if session['perm'] != "MD":
        return redirect(url_for('playlist'))
    return render_template('admin_page.html')

if __name__ == '__main__':
    application.run()
