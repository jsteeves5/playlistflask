-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: playlist
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `2`
--

DROP TABLE IF EXISTS `2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `2` (
  `track` int(11) NOT NULL DEFAULT '0',
  `song` varchar(50) DEFAULT NULL,
  `plays` int(11) DEFAULT NULL,
  PRIMARY KEY (`track`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `2`
--

LOCK TABLES `2` WRITE;
/*!40000 ALTER TABLE `2` DISABLE KEYS */;
INSERT INTO `2` VALUES (1,'Shallow',0),(2,'Deaf Waltz',0),(3,'Ghost Particle',0),(4,'Charnel House',0),(5,'Escher',0);
/*!40000 ALTER TABLE `2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `3`
--

DROP TABLE IF EXISTS `3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3` (
  `track` int(11) NOT NULL DEFAULT '0',
  `song` varchar(50) DEFAULT NULL,
  `plays` int(11) DEFAULT NULL,
  PRIMARY KEY (`track`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `3`
--

LOCK TABLES `3` WRITE;
/*!40000 ALTER TABLE `3` DISABLE KEYS */;
INSERT INTO `3` VALUES (1,'Golden Key',0),(2,'Drive',0),(3,'Summer\'s Gone',0),(4,'When with You',0),(5,'Personal Autumn',0),(6,'Heaven and Hell',0),(7,'Bathed in Blue',0),(8,'A Beautiful Space',0);
/*!40000 ALTER TABLE `3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `4`
--

DROP TABLE IF EXISTS `4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `4` (
  `track` int(11) NOT NULL DEFAULT '0',
  `song` varchar(50) DEFAULT NULL,
  `plays` int(11) DEFAULT NULL,
  PRIMARY KEY (`track`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `4`
--

LOCK TABLES `4` WRITE;
/*!40000 ALTER TABLE `4` DISABLE KEYS */;
INSERT INTO `4` VALUES (1,'Dusk',0),(2,'Molotov',0),(3,'Red Wine and Flowers',0),(4,'Deep Cuts',0),(5,'Boxing Day',0);
/*!40000 ALTER TABLE `4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `5`
--

DROP TABLE IF EXISTS `5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `5` (
  `track` int(11) NOT NULL DEFAULT '0',
  `song` varchar(50) DEFAULT NULL,
  `plays` int(11) DEFAULT NULL,
  PRIMARY KEY (`track`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `5`
--

LOCK TABLES `5` WRITE;
/*!40000 ALTER TABLE `5` DISABLE KEYS */;
INSERT INTO `5` VALUES (1,'Else',0),(2,'Balloon',0),(3,'Warp',0),(4,'Square',0),(5,'Inside The Silence',0),(6,'Angled',0),(7,'D',0),(8,'Prism',0),(9,'Black Box',0),(10,'Zero',0);
/*!40000 ALTER TABLE `5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_items`
--

DROP TABLE IF EXISTS `playlist_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album` varchar(100) NOT NULL,
  `artist` varchar(100) NOT NULL,
  `plays` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `local` tinyint(1) DEFAULT NULL,
  `current` tinyint(1) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_items`
--

LOCK TABLES `playlist_items` WRITE;
/*!40000 ALTER TABLE `playlist_items` DISABLE KEYS */;
INSERT INTO `playlist_items` VALUES (2,'If There Is A God That Is Judging Me Constantly','Up River',0,'Holy Roar','Brighton, UK',0,1,'11-24-2016'),(3,'Door to the Sun','The Stargazer Lilies',0,'Graveface','Savannah, Georgia',0,1,'06-03-2016'),(4,'Dusk','Sigh Gone',0,'self released','Australia',0,1,'11-21-2016'),(5,'Cubic','LITE',0,'Topshelf','Tokyo, Japan',0,1,'11-16-2016');
/*!40000 ALTER TABLE `playlist_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  `request` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'scoby','jsteeves@college.harvard.edu','pbkdf2:sha1:1000$KrtpqfCR$618b9a21dbbf84c42e48c3691bae9f8f03d69a88','MD',1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-28 15:04:29
