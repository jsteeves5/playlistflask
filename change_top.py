import json
import sys
import top_songs

args = sys.argv
prev = args[1]


with open('static/archives/' + prev, 'r') as f:
    prev_data = json.load(f)
f.closed

chart = top_songs.top20
prev_chart = prev_data['top-songs']
for i, entry in enumerate(chart):
    album, artist = entry[1], entry[2]
    prev_ranking = 0
    for j, prev_entry in enumerate(prev_chart):
        if album == prev_entry[1] and artist == prev_entry[2]:
            prev_ranking = j + 1

    if not prev_ranking:
        prev_ranking = " - "

    entry.append(str(prev_ranking))

print chart
