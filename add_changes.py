import json
import sys
import top_songs

args = sys.argv
prev = args[1]

with open('static/archives/' + prev, 'r') as f:
    prev_data = json.load(f)
f.closed

prev_chart = prev_data['top-songs']

try:
    this = args[2]
except:
    chart = top_songs.top20
    fname = "top_songs.py"
else:
    with open('static/archives/' + this, 'r') as f:
        data = json.load(f)
    f.closed

    chart = data['top-songs']
    fname = 'static/archives/' + this

for i, entry in enumerate(chart):
    album, artist = entry[1], entry[2]
    prev_ranking = 0
    for j, prev_entry in enumerate(prev_chart):
        if album == prev_entry[1] and artist == prev_entry[2]:
            prev_ranking = j + 1

    if not prev_ranking:
        prev_ranking = "-"

    entry.append(str(prev_ranking))

try:
    data['top-songs'] = chart
except:
    pass

if fname == "top_songs.py":
    with open(fname, 'w') as f:
        f.write('end = "{}"\n'.format(top_songs.end))
        f.write('start = "{}"\n'.format(top_songs.start))
        f.write('top20 = {}\n'.format(str(chart)))
    f.closed
else:
    with open(fname, 'w') as f:
        json.dump(data, f)
    f.closed
